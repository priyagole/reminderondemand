package com.example.priyanka.reminderondemand;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class MainActivity extends AppCompatActivity {

    private EditText memberId, phoneNumber;
    private Spinner spinnerMode,spinnerWay,spinnerServiceType, spinnerDialer, spinnerCountryCode;
    private Button buttonSend, buttonCancel;
    private ProgressDialog mProgressDialog;
    private static final String NAMESPACE = "http://tempuri.org/";
    private static final String METHOD_NAME = "ScheduleReminder";
    private static final String SOAP_ACTION = "http://tempuri.org/ScheduleReminder";
    private static final String URL = "http://tpancare.panhealth.com/Purchase_Reminder/Purchase_Reminder.asmx";
    private String [] spinnerDialorOptions = {"Select Dialer Option", "Twilio", "PanHealth"};
    private String [] spinnerCountryCodeOptions = {"+91", "+1"};
    private String [] spinnerWayOptions = {"Select Ways", "OutBound", "InBound", "TwoWay"};
    private String [] spinnerModeOptions = {"Select Mode", "IVR", "SMS", "EMAIL"};
    private String [] spinnerServiceOptions = {"Select Service Type", "BG", "BP", "Weight", "Medication", "Appointment", "HealthDiary"};
    private String source = "Android";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        memberId = findViewById(R.id.memberId);

        initView();

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new scheduleReminder().execute();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phoneNumber.setText("");
                memberId.setText("");
                spinnerDialer.setSelection(0);
                spinnerMode.setSelection(0);
                spinnerServiceType.setSelection(0);
                spinnerWay.setSelection(0);
                spinnerCountryCode.setSelection(0);
            }
        });
        ArrayAdapter dialer = new ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerDialorOptions);
        dialer.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDialer.setAdapter(dialer);

        ArrayAdapter ways = new ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerWayOptions);
        ways.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerWay.setAdapter(ways);

        ArrayAdapter mode = new ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerModeOptions);
        mode.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMode.setAdapter(mode);

        ArrayAdapter serviceTYpe = new ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerServiceOptions);
        serviceTYpe.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerServiceType.setAdapter(serviceTYpe);

        ArrayAdapter countryCode = new ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerCountryCodeOptions);
        countryCode.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCountryCode.setAdapter(countryCode);
    }

    private void initView() {
        memberId = findViewById(R.id.memberId);
        phoneNumber = findViewById(R.id.phoneNumber);
        spinnerMode = findViewById(R.id.spinnerMode);
        spinnerWay = findViewById(R.id.spinnerWay);
        spinnerServiceType = findViewById(R.id.spinnerServiceType);
        spinnerDialer = findViewById(R.id.spinnerDialer);
        spinnerCountryCode = findViewById(R.id.spinnerCountryCode);
        buttonSend = findViewById(R.id.buttonSend);
        buttonCancel = findViewById(R.id.buttonCancel);
    }

    private class scheduleReminder extends AsyncTask<Void, Void, Void> {

        boolean resultFlag;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            mProgressDialog = new ProgressDialog(MainActivity.this);
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
                Log.e("Request" , request.toString());

                request.addProperty("memberId", memberId.getText().toString());
                request.addProperty("mode", spinnerMode.getSelectedItem().toString());
                request.addProperty("way", spinnerWay.getSelectedItem().toString());
                request.addProperty("serviceType", spinnerServiceType.getSelectedItem().toString());
                request.addProperty("language", "English");
                request.addProperty("phoneNumber", phoneNumber.getText().toString());
                request.addProperty("dialer", spinnerDialer.getSelectedItem().toString());
                request.addProperty("Countryprefix ", spinnerCountryCode.getSelectedItem().toString());
                request.addProperty("Source ", source);
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                        SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);
                Log.e("request : " , request.toString());

                envelope.encodingStyle = SoapSerializationEnvelope.XSD;
                Log.e("envelope : " , envelope.toString());
                HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);


                try {
                    androidHttpTransport.call(SOAP_ACTION, envelope);
                    Object result = (Object) envelope.getResponse();
                    Log.e("result call :" , result.toString());
                    String Result = result.toString();

                    Log.d("Response", Result);

                    resultFlag = true;
                } catch (SoapFault f) {
                    Log.d("Error", f.toString());
                    resultFlag = false;


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            if (null != mProgressDialog && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }

            if (resultFlag == true) {

                Toast.makeText(MainActivity.this, "Reminder scheduled successfully.", Toast.LENGTH_SHORT).show();
            }
            if (resultFlag == false) {
                Toast.makeText(MainActivity.this, "Reminder doesn't scheduled successfully.", Toast.LENGTH_SHORT).show();
            }
        }
    }


    /*private class scheduleReminder extends AsyncTask<Void, Void, Void> {

        private String Content;
        private String Error = null;
        boolean resultFlag;
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(MainActivity.this);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
        @Override
        public Void doInBackground(Void... voids) {

            try {
                OkHttpClient client = new OkHttpClient();

                Request request = new Request.Builder()
                        .url("http://tpancare.panhealth.com/Purchase_Reminder/Purchase_Reminder.asmx")
                        .build();
                }catch ()
                return null;
            *//*JSONfunctions js = new JSONfunctions();
            JSONObject jsonObjSend = new JSONObject();
            try {

                jsonObjSend.put("memberId", memberId.getText().toString());
                jsonObjSend.put("mode", spinnerMode.getSelectedItem().toString());
                jsonObjSend.put("way", spinnerWay.getSelectedItem().toString());
                jsonObjSend.put("serviceType", spinnerServiceType.getSelectedItem().toString());
                jsonObjSend.put("language", "English");
                jsonObjSend.put("phoneNumber", phoneNumber.getText().toString());
                jsonObjSend.put("dialer", spinnerDialer.getSelectedItem().toString());


                Log.e("input", jsonObjSend.toString().toString());
                String response = js.getJSONfromURL(
                        "http://tpancare.panhealth.com/Purchase_Reminder/Purchase_Reminder.asmx" , jsonObjSend);

                Log.e("Response", response.toString());

                if (response != null) {

                    JSONObject json = new JSONObject(response);
                    JSONArray responseData = null;
                    int statusCode;
                    statusCode = json.getInt("");
                    responseData = json.optJSONArray("");
                    if (responseData != null) {
                        if(statusCode == 200) {
                            resultFlag = true;
                        }else{
                            resultFlag = false;
                        }
                    } else {
                        resultFlag = false;
                    }
                }

            } catch (JSONException e) {

                resultFlag = false;
            }

            return null;*//*
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (null != mProgressDialog && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }

            if (resultFlag == true) {

                Toast.makeText(MainActivity.this, "Reminder scheduled successfully.", Toast.LENGTH_SHORT).show();
            }
            if (resultFlag == false) {
                Toast.makeText(MainActivity.this, "Reminder doesn't scheduled successfully.", Toast.LENGTH_SHORT).show();
            }
        }
    }*/
}
